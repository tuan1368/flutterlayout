import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Now we need multiple widgets into a parent = "Container" widget
    // Widget titleSection = new Text("This is a title section");
    Widget titleSection = new Container(
      padding: const EdgeInsets.all(10.0),
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: new Text(
                    "Lavender field in France",
                    style: new TextStyle(
                        fontSize: 18.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                // Need to add some space below this Text
                new Text(
                  "This chanel contains my picture in France",
                  style: new TextStyle(color: Colors.grey[850], fontSize: 16.0),
                )
              ],
            ),
          ),
          new Icon(Icons.favorite, color: Colors.red),
          new Text(
            " 100",
            style: new TextStyle(fontSize: 16.0),
          )
        ],
      ),
    );

    Widget buildButton(IconData icon, String buttonTitle) {
      final Color tintColor = Colors.blue;
      return new Column(
        children: <Widget>[
          new Icon(icon, color: tintColor),
          new Container(
              margin: const EdgeInsets.only(top: 5.0),
              child: new Text(buttonTitle,
                  style: new TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                      color: tintColor))),
        ],
      );
    }

    // Build function to return a widget
    Widget forButtonSections = new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          buildButton(Icons.home, "Home"),
          buildButton(Icons.arrow_back, "Back"),
          buildButton(Icons.arrow_forward, "Next"),
          buildButton(Icons.share, "Share"),
        ],
      ),
    );
    final bottomTextSections = new Container(
      padding: const EdgeInsets.all(10.0),
      child: new Text('''For tourists as well as locals, lavender is more than just a plant. The Provence lavender fields are part of the decor, and contribute's to the region's renown around the world. Does your French vacation include a stop in the South? Discover all the secrets of this plant, whose recognizable scent both fascinates and enchants: the history of its cultivation, the most beautiful fields of lavender in bloom of all Provence, the flowering seasons when the display is at its best.
      Lavender, prized for its medicinal qualities, known for its ability to repel certain insects, and beloved for its fresh and floral scent that is both aromatic and lightly medicinal, has been picked in the wild by Provence locals since time immemorial.  But it wasn't until the 19th century that poor Haute-Provence peasants saw its potential as a source of wealth worth cultivating and exporting. The first fields of lavender in Provence, as we know them today, date back to that era.
Several species can be cultivated in Provence lavender fields. In fact, we make a distinction between "true lavender", a noble plant protected since 1981 by a controlled designation of origin, and  lavandin, a hybrid which produces more but is considered less noble and of lower quality. Often, these plants are turned into essential oils or are dried. They also serve as aromas for food recipes.''',
      style: new TextStyle(color: Colors.grey[850],
      fontSize: 16.0)),
    );
    return new MaterialApp(
      title: "Flutter Layout demo",
      home: new Scaffold(
          appBar: new AppBar(
            title: new Center(
              child: new Text("Lavender Field"),
            ),
          ),
          body: new ListView(
            children: <Widget>[
              new Image.asset(
                'images/lavenderfield.jpg',
                fit: BoxFit.cover,
              ),
              // You can add more widget below
              titleSection,
              forButtonSections,
              bottomTextSections
            ],
          )),
    );
  }
}
